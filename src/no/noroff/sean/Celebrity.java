package no.noroff.sean;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Celebrity {
    private String name;
    private List<String> controversies = List.of("some controversy", "another controversy", "an completely different controversy");

    private List<Follower> followers = new ArrayList<>();

    public Celebrity(String name) {
        this.name = name;
    }

    public void subscribe(Follower follower) {
        followers.add(follower);
    }

    public void unsubscribe(Follower follower) {
        followers.remove(follower);
    }

    private void notifyFollowers(String controversyDetails) {
        for (var follower:followers) {
            follower.notify(controversyDetails);
        }
    }

    public void generateNewControversy() {
        Random random = new Random();
        notifyFollowers(name + " " + controversies.get(random.nextInt(controversies.size())));
    }
}
