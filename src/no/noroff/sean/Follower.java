package no.noroff.sean;

public interface Follower {
    void notify(String message);
}
