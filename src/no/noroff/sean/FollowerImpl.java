package no.noroff.sean;

import java.util.List;
import java.util.Random;

public class FollowerImpl implements Follower {
    private String username;

    private static List<String> responses = List.of("What a scandal!", "Outrageous!", "How could they?!");

    public FollowerImpl(String username) {
        this.username = username;
    }

    @Override
    public void notify(String message) {
        var random = new Random();
        postOnPlatform(username + ": " + message + "..." + responses.get(random.nextInt(responses.size())));
    }

    public void postOnPlatform(String details) {
        System.out.println(details);
    }
}
