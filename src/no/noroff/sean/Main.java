package no.noroff.sean;

public class Main {

    public static void main(String[] args) {
	    Celebrity superFamousPerson = new Celebrity("Joe Doe");
        Celebrity anotherFamousPerson = new Celebrity("Jane Doe");

        var follower1 = new FollowerImpl("MirandaPriestly123");
        var follower2 = new FollowerImpl("LadyWhistledown");

        superFamousPerson.subscribe(follower1);
        superFamousPerson.subscribe(follower2);

        superFamousPerson.generateNewControversy();
        superFamousPerson.generateNewControversy();
        superFamousPerson.generateNewControversy();

        anotherFamousPerson.subscribe(follower1);

        anotherFamousPerson.generateNewControversy();
        anotherFamousPerson.generateNewControversy();
        anotherFamousPerson.generateNewControversy();

        superFamousPerson.unsubscribe(follower1);

        superFamousPerson.generateNewControversy();
    }
}
